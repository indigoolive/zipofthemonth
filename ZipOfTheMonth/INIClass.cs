using System;
using System.Runtime.InteropServices;
using System.Text;

using System.IO;

using LOGGING;

namespace INI
{
	/// <summary>
	/// Summary description for INIClass.
	/// </summary>
	public class INIFile
	{    
		public string path;
		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
		
		public INIFile()
		{
            //path = Path.GetDirectoryName(Application.ExecutablePath )+ @"\DelFilesOlderThan.ini";
            path = Directory.GetCurrentDirectory() + @"\ZipOfTheMonth.ini";
            
            if (!(File.Exists(path)))
            {
                Logger log = new Logger();
                log.WriteError("{INI.INIFile()} .ini not found - best to abort");
                Environment.Exit(0);
            }   
		} //constructor   
		
		public string INIReadString(string Section,string Key)
		{
			StringBuilder temp = new StringBuilder(255);
			int i = GetPrivateProfileString(Section,Key,"",temp,255, this.path);
			return temp.ToString();
		}

		public int INIReadInt(string Section,string Key)
		{
			StringBuilder temp = new StringBuilder(255);
			int i = GetPrivateProfileString(Section,Key,"",temp,255, this.path);

			return Convert.ToInt32(temp.ToString());
		}
		 
		public void INIWriteValue(string Section, string Key, string Value)//overloaded
		{        
			WritePrivateProfileString(Section, Key, Value, this.path);
		}    
		
		public void INIWriteValue(string Section, string Key, int Value)//overloaded
		{        
			INIWriteValue(Section, Key, Value.ToString());
		}   

	}//class

}//namespace
