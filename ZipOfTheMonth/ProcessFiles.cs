﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;

using System.IO;

using Ionic.Zip;

using LOGGING;
using INI;

namespace ZipOfTheMonth
{
    class ProcessFiles
    {
        public void ZipSomeFiles(string INISection)
        {
            string SingleSlash = @"\";

            string[] ListOfFiles;
            string curdir;

            DateTime DeleteTime;
            DateTime Creation;
            int FileCount = 0;

            int ZipCount = 0;
            bool ZipHappened;

            int SaveCount = 0;
            bool ZipSaveHappened;

            bool bDelWhenZipped;
            int DelCount = 0;
            bool DelHappened;

            string ZipFilenameSuffix;
            string ZipFilenameFull;

            Console.WriteLine("Starting");

            INIFile ini = new INIFile();
            Logger log = new Logger();

            log.WriteInfo("Beginning ZipSomeFiles");

            //There had better be an INISection
            if (INISection == "")
            {
                log.WriteError("You must specify an INISection as a command-line argument.");
                return;
            }

            //Read the .ini files; report what you find
            int DaysNotToZip = ini.INIReadInt(INISection, "DaysNotToZip");
            string FilePath = ini.INIReadString(INISection, "FilePath");
            string FileSpec = ini.INIReadString(INISection, "FileSpec");
            string ZipFilenamePrefix = ini.INIReadString(INISection, "ZipFilenamePrefix");
            int DeleteWhenZipped = ini.INIReadInt(INISection, "DeleteWhenZipped");

            log.WriteInfo("{ZOTM} I'm using INISection: " + INISection);
            log.WriteInfo("{ZOTM} Number of days not to zip: " + DaysNotToZip.ToString());
            log.WriteInfo("{ZOTM} The filepath is: " + FilePath);
            log.WriteInfo("{ZOTM} The filespec is: " + FileSpec);
            log.WriteInfo("{ZOTM} The zip filename prefix is: " + ZipFilenamePrefix);
            log.WriteInfo("{ZOTM} DeleteWhenZipped is: " + DeleteWhenZipped.ToString());

            //Double-check
            if ((FilePath == "") || (!(Directory.Exists(FilePath))))
            {
                log.WriteError("{ZOTM} Bad/missing path, is INISection correct?");
                return;
            }

            if (FilePath.Substring(FilePath.Length - 1) != SingleSlash)
            {
                log.WriteError("{ZOTM} FilePath must end with a " + SingleSlash);
                return;
            }

            if (FileSpec == "") //bad INISection, missing or FileSpec
            {
                log.WriteError("{ZOTM} Bad/missing filespec, is INISection correct?");
                return;
            }

            if (ZipFilenamePrefix == "")
            {
                log.WriteError("{ZOTM} Bad/missing ZipFilenamePrefix?");
                return;
            }

            if (DeleteWhenZipped != 1) //Force to zero
            {
                if (DeleteWhenZipped != 0) //Report the forcing since we didn't specify zero
                {
                    log.WriteError("{ZOTM} Bad/missing DeleteWhenZipped - forced to zero [no delete].");
                }
                DeleteWhenZipped = 0; //If it isn't 1 force it to zero even if it was zero
            }

            //Make some calculation based on the .ini
            DeleteTime = DateTime.Today.AddDays(-1 * DaysNotToZip);
            log.WriteInfo("{ZOTM} The current delete before date is " + DeleteTime.ToLongDateString());

            bDelWhenZipped = (DeleteWhenZipped == 1);
            
            //Change to working directory
            //Save current directory so you can swith back when you exit [perhaps extraneous?]
            curdir = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(FilePath);
            log.WriteInfo("{ZOTM} Changed to: " + FilePath);

            ListOfFiles = Directory.GetFiles(Directory.GetCurrentDirectory(),FileSpec);

            foreach (string fp in ListOfFiles)
            {
                Creation = File.GetCreationTime(fp);

                if (Creation < DeleteTime)
                {
                    FileCount++;
                    //Ok we have a file to zip
                    //But what zip file should it go in
                    ZipFilenameSuffix = "." + Creation.ToString("yyyyMM") + ".zip"; //eg Monthly.2012JAN.zip
                    ZipFilenameFull = ZipFilenamePrefix + ZipFilenameSuffix;

                    ZipFile zip = new ZipFile();

                    //Create appropriate zip object
                    if (File.Exists(ZipFilenameFull))
                    {
                       zip = ZipFile.Read(ZipFilenameFull);
                    }
                   
                    //Add file to zip
                    ZipHappened = true;

                    try
                    {
                        zip.AddFile(Path.GetFileName(fp)); //simple file name so we don't store dirs.
                    }
                    catch (Exception e1)
                    {
                        ZipHappened = false;
                        zip.Dispose(); //Cleanup
                        log.WriteError("{ZOTM} Unable to zip " + fp);
                        log.WriteError("{ZOTM} Reason was " + e1.ToString());
                    }

                    if (ZipHappened)
                    {
                        ZipCount++;

                        ZipSaveHappened = true;
                        try
                        {
                            zip.SortEntriesBeforeSaving = true;
                            zip.Save(ZipFilenameFull);
                        }
                        catch (Exception e2)
                        {
                            ZipSaveHappened = false;
                            zip.Dispose(); //Cleanup
                            log.WriteError("{ZOTM} Unable to save the zip file " + ZipFilenameFull);
                            log.WriteError("{ZOTM} Reason was " + e2.ToString());
                        }
                        
                        if (ZipSaveHappened)
                        {
                            SaveCount++;

                            log.WriteInfo("{ZOTM} Zipped file " + fp + " to " + ZipFilenameFull);

                            zip.Dispose(); //Cleanup

                            //Now delete if .ini says to
                            if (bDelWhenZipped)
                            {
                                DelHappened = true;

                                try
                                {
                                    File.Delete(fp);
                                }
                                catch (Exception e3)
                                {
                                    DelHappened = false;
                                    log.WriteError("{ZOTM} Unable to delete " + fp);
                                    log.WriteError("{ZOTM} Reason was " + e3.ToString());
                                }

                                if (DelHappened)
                                {
                                    log.WriteInfo("{ZOTM} Deleted file: " + fp);
                                    DelCount++;
                                }
                            } //bDelWhenZipped
                        } //save was successful
                    } //zip was successful
                } //file is old enough to be zipped

                if (FileCount % 10 == 0) //modulus
                        Console.Write(".");

            } //for each file matching spec
         
            Console.WriteLine(); //end the dots

            //Summarize
            log.WriteInfo("{ZOTM} Total files matching FileSpec: " + ListOfFiles.GetLength(0).ToString());
            log.WriteInfo("{ZOTM} Total files old enough to zip: " + FileCount.ToString());
            log.WriteInfo("{ZOTM} Total files compressed: " + ZipCount.ToString());
            log.WriteInfo("{ZOTM} Total files saved to a .zip file: " + SaveCount.ToString());

            if (FileCount != ZipCount)
                log.WriteError("{ZOTM} Why does FileCount <> ZipCount ???");

            if (ZipCount != SaveCount)
                log.WriteError("{ZOTM} Why does ZipCount <> SaveCount ???");

            if (bDelWhenZipped)
            {
                log.WriteInfo("{ZOTM} Total files deleted: " + DelCount.ToString());

                if (DelCount != SaveCount)
                    log.WriteError("{ZOTM} Why does DelCount <> SaveCount ???");
            }

            //Change back to original subdir
            Directory.SetCurrentDirectory(curdir);

            log.WriteInfo("Done ZipSomeFiles");

            Console.WriteLine("Done");
        }

    }//class 
}//namespace
